xfonts-biznet (3.0.0-26) unstable; urgency=medium

  * Set `Rules-Requires-Root' to `no'.
  * Remove `xfs' from Suggests and Description fields of binary packages,
    as xfs package was removed from Debian a few years ago.
  * Standards-Version: 4.2.0.

 -- Robert Luberda <robert@debian.org>  Sun, 05 Aug 2018 10:00:06 +0200

xfonts-biznet (3.0.0-25) unstable; urgency=medium

  * Build with debhelper v11.
  * Mark the binary packages as `Multi-Arch: foreign'.
  * Switch Vcs-* fields to salsa.debian.org.
  * Standards-Version: 4.1.4.

 -- Robert Luberda <robert@debian.org>  Mon, 02 Jul 2018 22:59:56 +0200

xfonts-biznet (3.0.0-24) unstable; urgency=medium

  * Use `gzip -n' to avoid timestamps in compressed files.
  * Switch debian/copyright to DEP-5 format.
  * Use https for Vcs-Git/Browser fields, and switch the latter field to
    point to canonical cgit URL.
  * Bump Standards-Version to 3.9.7 (no changes).

 -- Robert Luberda <robert@debian.org>  Sun, 13 Mar 2016 22:13:00 +0100

xfonts-biznet (3.0.0-23) unstable; urgency=low

  * Switch to debhelper v9.
  * Generate pcf files in a separate build directory.
  * Standards-Version: 3.9.4 (no changes).
  * Remove yada's debian/{packages,packages.common} files.
  * Add Vcs-* fields.

 -- Robert Luberda <robert@debian.org>  Sat, 25 May 2013 14:59:10 +0200

xfonts-biznet (3.0.0-22) unstable; urgency=low

  * Use debhelper v8 instead of yada (closes: #636892).
  * Standards-Version: 3.9.2 (no changes).
  * Drop ancient preinst script.

 -- Robert Luberda <robert@debian.org>  Tue, 09 Aug 2011 07:57:31 +0200

xfonts-biznet (3.0.0-21) unstable; urgency=low

  * Switch to the `3.0 (quilt)' format.
  * Standards-Version: 3.9.1 (no changes).
  * postrm: After removing fonts.{dir,alias} files remove any empty parent
    directiories. This really should fix issues found by piuparts.
  * Remove any Conflicts/Replaces the binary packages had against ancient
    packages.

 -- Robert Luberda <robert@debian.org>  Tue, 15 Feb 2011 22:13:18 +0100

xfonts-biznet (3.0.0-20) unstable; urgency=low

  * postrm: Remove fonts.{dir,alias} files by hand if update-fonts-{dir,alias}
    commands are missing. This fixes a problem found by piuparts.
  * Change package section to fonts.
  * Standards-Version: 3.8.3.

 -- Robert Luberda <robert@debian.org>  Thu, 10 Sep 2009 18:52:32 +0200

xfonts-biznet (3.0.0-19) unstable; urgency=low

  * Fix typo in the postinst script (closes: #391896).
  * Don't recommend unavailable xfonts-base-transcoded package
    (closes: #376916).
  * Move alias and dirs files back to /etc/X11/fonts.
  * Drop the --x11r7-layout option from update-fonts-{dirs,alias}
    invocations.
  * Standards-Version: 3.7.2.

 -- Robert Luberda <robert@debian.org>  Wed,  3 Jan 2007 00:23:32 +0100

xfonts-biznet (3.0.0-18) unstable; urgency=low

  * X fonts transition (closes: #362346, #362348, #362356):
    + install fonts in /usr/share/fonts/X11
    + install alias and dirs files in /etc/X11/fonts/X11R7
    + postinst,postrm: run update-fonts-{dirs,alias} with the --x11r7-layout
      flag
    + add dependencies and build-dependencies on xfonts-utils.
  * Split common parts of debian/packages into debian/packages.common.
  * Bump Standards-Version to 3.7.0.

 -- Robert Luberda <robert@debian.org>  Sat, 29 Apr 2006 15:36:06 +0200

xfonts-biznet (3.0.0-17) unstable; urgency=low

  * Adopting package (closes: #237476).
  * Rename the source package. The previous name was rather misleading since
    binary packages' names no longer contain the 'iso-8859-2' part.
  * Build with newest yada so the package should comply with the newest
    Standards-Version (i.e. 3.6.2).

 -- Robert Luberda <robert@debian.org>  Sat, 16 Jul 2005 22:37:22 +0200

xfonts-biznet-iso-8859-2 (3.0.0-16) unstable; urgency=low

  * QA upload.
  * Remove the old, transitional binary packages (closes:  #308717).
  * Correct packages names in descriptions.
  * Remove unnecessary Origin field (lintian).
  * Rename readme.gz files to README.gz.

 -- Robert Luberda <robert@debian.org>  Tue,  7 Jun 2005 21:06:41 +0200

xfonts-biznet-iso-8859-2 (3.0.0-15) unstable; urgency=low

  * Uploading with maintainer set to QA group
  * debian/control: Removed Bugs: field (closes: #220068, #220076, #220083,
    #220100, #220101, #220104)
  * debian/control: Fixed case of Origin: field contents (closes: #154418)
  * (actually did all of the above to debian/packages because it uses %$!@ing
    yada)
  * debian/packages: Removed period from end of package descriptions

 -- Andrew Pollock <apollock@debian.org>  Sun, 18 Apr 2004 17:19:06 +1000

xfonts-biznet-iso-8859-2 (3.0.0-14) unstable; urgency=low

  * ISO-8859-2 doesn't support Romanian language, closes: #119534, #119535

 -- Piotr Roszatycki <dexter@debian.org>  Wed, 21 Nov 2001 17:02:10 +0100

xfonts-biznet-iso-8859-2 (3.0.0-13) unstable; urgency=high

  * Comment out old conffiles, closes: #91852, #116459, #116361

 -- Piotr Roszatycki <dexter@debian.org>  Mon, 22 Oct 2001 16:08:16 +0200

xfonts-biznet-iso-8859-2 (3.0.0-12) unstable; urgency=low

  * Old packages clean their outdated conffiles, closes: #114331

 -- Piotr Roszatycki <dexter@debian.org>  Tue, 16 Oct 2001 15:21:06 +0200

xfonts-biznet-iso-8859-2 (3.0.0-11) unstable; urgency=low

  * Fonts are placed in new packages. The old packages are
    provided for backward compability.
  * New packages should resolve problems with xfonts-*-transcoded,
    closes: #108067, #108911

 -- Piotr Roszatycki <dexter@debian.org>  Wed, 12 Sep 2001 20:14:49 +0200

xfonts-biznet-iso-8859-2 (3.0.0-10) unstable; urgency=low

  * Removed fonts which are packaged in xfonts-*-transcoded
  * Recommends: xfonts-*-transcoded

 -- Piotr Roszatycki <dexter@debian.org>  Fri,  3 Aug 2001 10:46:03 +0200

xfonts-biznet-iso-8859-2 (3.0.0-9) unstable; urgency=low

  * No font conflicts with xfonts-base anymore.
  * Conflicts: xfonts-{base,75dpi,100dpi} (<< 4.1.0)

 -- Piotr Roszatycki <dexter@debian.org>  Wed,  1 Aug 2001 09:45:09 +0200

xfonts-biznet-iso-8859-2 (3.0.0-8) unstable; urgency=low

  * New yada and clean up packages
  * New Standards-Version: 3.5.0
  * Removed fonts which conficted with xfonts-base (>= 4.0.1), closes: #79226

 -- Piotr Roszatycki <dexter@debian.org>  Wed,  7 Feb 2001 17:31:25 +0100

xfonts-biznet-iso-8859-2 (3.0.0-7) frozen unstable; urgency=low

  * Removed entries for scalable fonts from aliases.fonts,
    as far as it broken xfig, closes: #62038
  * Fixed description for binary packages, closes: #61870

 -- Piotr Roszatycki <dexter@debian.org>  Mon, 17 Apr 2000 16:43:41 +0200

xfonts-biznet-iso-8859-2 (3.0.0-6) unstable; urgency=medium

  * 'find' replaced by 'find $currentdir' in postrm scripts,
    closes: #49764, #49765, #49766
  * Standards-Version: 3.1.0
  * yada 0.8

 -- Piotr Roszatycki <dexter@debian.org>  Wed, 10 Nov 1999 16:05:08 +0100

xfonts-biznet-iso-8859-2 (3.0.0-5) unstable; urgency=low

  * Some minor changes in debian/packages.
  * Standards-Version: 3.0.1

 -- Piotr Roszatycki <dexter@debian.org>  Fri, 22 Oct 1999 14:05:36 +0200

xfonts-biznet-iso-8859-2 (3.0.0-4) unstable; urgency=low

  * Depends: xbase-clients (>= 3.3.3.1-5).
  * Deleted Pre-Depends: xfonts-{base|100dpi|75dpi}.
  * Adaptated update-fonts-alias mechanism for control scripts.
  * Standards-Version: 3.0.0.0
  * yada 0.7.2 and debhelper

 -- Piotr Roszatycki <dexter@debian.org>  Sat, 24 Jul 1999 22:50:42 +0200

xfonts-biznet-iso-8859-2 (3.0.0-3) unstable; urgency=low

  * Generated md5sums

 -- Piotr Roszatycki <dexter@debian.org>  Wed, 12 May 1999 01:03:23 +0200

xfonts-biznet-iso-8859-2 (3.0.0-2) unstable; urgency=low

  * Included homepage and download URL
  * Extended package description.
  * Rewritten (pre|post)(rm|inst) scripts.
  * Package name changed to new scheme
    (xfonts-<producer>-<fontname>-<encoding>-<size>).
  * yada technology.

 -- Piotr Roszatycki <dexter@debian.org>  Sun,  9 May 1999 12:57:32 +0200

xfonts-latin2-biznet (3.0.0-1) unstable; urgency=low

  * Corrected version number (included maintainer's subversion)

 -- Piotr Roszatycki <dexter@debian.org>  Wed, 21 Apr 1999 13:20:30 +0200

xfonts-latin2-biznet (3.0.0) unstable; urgency=low

  * Autoconfiguration of fonts.alias file
  * $path/$font-il2/* migrated to $path/$font/*
  * Standards 2.5.0.0
  * Use debhelper.
  * Package name changed.
  * New maintainer.

 -- Piotr Roszatycki <dexter@debian.org>  Mon, 12 Apr 1999 18:53:39 +0200

xfntil2 (2.1.0) unstable; urgency=low

  * ETL fonts removed (available in the `intlfonts-european' now).
  * Maintainer address changed.
  * Standards 2.1.4.
  * `README.Debian' updated.
  * `postrm' actions performed only for "remove".

 -- Milan Zamazal <pdm@debian.org>  Sat, 10 Oct 1998 13:19:46 +0200

xfntil2 (2.0.4) unstable; urgency=low

  * Use `dh_md5sums'.
  * Lintian 0.2.2 satisfied.

 -- Milan Zamazal <pdm@fi.muni.cz>  Wed, 18 Feb 1998 19:18:55 +0100

xfntil2 (2.0.3) unstable; urgency=low

  * `checksums' renamed to `md5sums'.
  * Standards 2.4.0.0.

 -- Milan Zamazal <pdm@fi.muni.cz>  Fri, 13 Feb 1998 18:10:25 +0100

xfntil2 (2.0.2) unstable; urgency=low

  * `copyright' file is no more executable.
  * `-isp' flag added to dpkg-gencontrol.
  * Dot added after description synopsis in `control'.
  * `set -e' added to the font build script.
  * Gzip `changelog.Debian'.
  * Standards 2.3.0.1.

 -- Milan Zamazal <pdm@fi.muni.cz>  Mon, 12 Jan 1998 19:42:44 +0100

xfntil2 (2.0.1) unstable; urgency=low

  * Big new collection of fonts from BIZNET Poland, inc., added.
  * Font sets 2-4 removed.
  * Conflicts with xfntil2-nonfree now.
  * Installs into separate font directories now.
  * `rules' corrected according to indep/arch policy.

 -- Milan Zamazal <pdm@fi.muni.cz>  Tue, 21 Oct 1997 16:04:29 +0200

xfntil2 (1.0.3) unstable; urgency=low

  * Fonts are gzipped now.
  * Does not use `debstd'.
  * Depends on X now.
  * `copyright' file is not gziped anymore.

 -- Milan Zamazal <pdm@fi.muni.cz>  Thu,  7 Aug 1997 10:24:10 +0200

xfntil2 (1.0.2) unstable; urgency=low

  * Corrected typo crippling `postinst' script.

 -- Milan Zamazal <pdm@fi.muni.cz>  Mon, 10 Mar 1997 11:29:20 +0100

xfntil2 (1.0.1) unstable; urgency=low

  * `postinst' and `postrm' scripts check for `mkfontdir' now.
  * Fixed bad permissions.
  * Standards 2.1.2.2.

 -- Milan Zamazal <pdm@fi.muni.cz>  Mon, 24 Feb 1997 14:44:14 +0100

xfntil2 (1.0) unstable; urgency=low

  * Initial Release.

 -- Milan Zamazal <pdm@fi.muni.cz>  Mon, 16 Dec 1996 13:09:05 +0100
